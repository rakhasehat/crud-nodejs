const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const MongoCilent = require('mongodb').MongoClient; //import driver mongodb
const ObjectID = require('mongodb').ObjectID; //import objectid
const DBurl = "mongodb://127.0.0.1:27017/"; //url db -> port mongo : 27017
const DBname = "db_tes";

//middleware bodyparser
//nb : diletakkan dibawah const atau setelah mendeklarasikan const
app.use(bodyParser.urlencoded({extended: false}))

let dbo = null; //object koneksi database
//koneksi database
MongoCilent.connect(DBurl, (error, db)=>{
    if(error) throw error;
    dbo = db.db(DBname);
});

//endpoint get : mengambil data dari database yang telah dibuat sebelumnya
app.get('/siswa', (request, response)=>{
    dbo.collection("siswa").find().toArray((err, res)=>{//mengambil data dari collection dalam bentuk array
        if(err) throw err;
        response.json(res);//menampilkan data
    })
});

//endpoint post : menambahkan data ke database
app.post('/siswa', (request, response)=>{
    let namaSiswa = request.body.nama;
    let alamatSiswa = request.body.alamat;
    dbo.collection("siswa").insertOne({
        nama: namaSiswa,
        alamat: alamatSiswa
    }, (err, res) => {
        if(!err){
            response.json(res);
            response.end("data berhasil masuk");
        }else{
            throw err;//apabila error akan dilempar ke nodejs
        }
    })
});

//endpoint delete : menghapus data
app.delete('/siswa/:id', (request, response)=>{
    let id = request.params.id;
    let id_object = new ObjectID(id);
    dbo.collection("siswa").deleteOne({ //perintah menghapus data
        _id: id_object //mengambil id data untuk dihapus
    }, (err, res) =>{
        if(err) throw err;
        response.end("data berhasil dihapus");
    })
})

//endpoint update data database
app.put('/siswa/:id', (request, response)=>{
    let id = request.params.id;
    let id_object = new ObjectID(id);
    let namaSiswa = request.body.nama;
    let kelasSiswa = request.body.kelas;
    let jurusanSiswa = request.body.jurusan;
    dbo.collection("siswa").updateOne({
        _id: id_object //mengambil data dengan id
    }, {$set:{         //proses update
        nama: namaSiswa,
        kelas: kelasSiswa,
        jurusan: jurusanSiswa
    }}, (err, res)=>{
        if(!err){
            response.end("data berhasil diupdate")
        }
        else{
            throw err;
        }
    }
    )
})

// //endpoint get dengan menggunakan parameter
// app.get('/siswa/:nama', (request, response)=>{ // :nama merupakan parameter
//     let namaSiswa = request.params.nama; // deklarasi variabel namasiswa
//     response.end("menampilkan nama siswa "+ namaSiswa);
// });

// //endpoint post, dengan menggunakan body-parser untuk mengirimkan data
// app.post('/siswa', (request, response)=>{
//     let namaSiswa = request.body.name;
//     let alamat = request.body.address;
//     response.end('menampilkan siswa baru ' + namaSiswa + ', yang beralamat di ' + alamat);
// });

// //endpoint delete, data yang diakses secara spesifik dengan menggunakan paramater
// app.delete('/siswa/:id', (request, response)=>{
//     let id = request.params.id;
//     let namaSiswa = request.body.nama;
//     response.end('id '+ id + ' telah dihapus, dengan nama: '+namaSiswa);
// });

// //endpoint update
// app.put('/siswa/:id', (request, response)=>{
//     let id = request.params.id;
//     let namaSiswa = request.body.nama;
//     let alamat = request.body.alamat;
//     response.end('siswa dengan id: '+id+' telah diupdate')
// });

// app.get('/tet', (req, res)=>{
//     res.send("oke oleh")
// })

app.listen('2000');